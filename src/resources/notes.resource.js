import appConfig from '../config/app.config'

import NoteModel from '../models/note.model'

export default (api) => {

    const getBookNotes = (bookId) => {
        let params = {
            bookId
        };
        return api.makeRequest(api.GET, appConfig.apiUrl + 'notes',params)
            .then(notes => {
                return notes.map(note => new NoteModel(note.id,note.bookId,note.note,note.createdAt));
            });
    };

    const addNote = (noteModel) => {
        let body = {
            bookId: noteModel.bookId,
            note: noteModel.note
        };
        return api.makeRequest(api.POST, appConfig.apiUrl + 'notes',undefined,body)
            .then(newNote => {
                return new NoteModel(newNote.id,newNote.bookId,newNote.note,newNote.createdAt);
            });
    };

    const updateNote = (noteModel) => {
        let body = {
            note: noteModel.note
        };
        return api.makeRequest(api.PUT, appConfig.apiUrl + 'notes/' + noteModel.id, undefined, body)
            .then(updatedNote => {
                return new NoteModel(updatedNote.id,updatedNote.bookId,updatedNote.note,updatedNote.createdAt);
            });
    };

    const deleteNote = (noteId) => {
        return api.makeRequest(api.DELETE, appConfig.apiUrl + 'notes/' + noteId);
    };

    return {
        getBookNotes,
        addNote,
        updateNote,
        deleteNote
    }
};
