import appConfig from '../config/app.config'

import CampaignModel from '../models/campaign.model'

export default (api) => {

    const getCampaigns = () => {
        return api.makeRequest(api.GET, appConfig.apiUrl + 'campaigns')
            .then(campaigns => {
                return campaigns.map(campaign =>  new CampaignModel(campaign));
            });
    };

    const addCampaign = (campaignModel) => {
        return api.makeRequest(api.POST, appConfig.apiUrl + 'campaigns',undefined,campaignModel)
            .then(newCampaign => {
                return new CampaignModel(newCampaign);
            });
    };

    const updateCampaign = (campaignModel) => {
        return api.makeRequest(api.PUT, appConfig.apiUrl + 'campaigns/' + campaignModel.id, undefined, campaignModel)
            .then(updatedCampaign => {
                return new CampaignModel(updatedCampaign);
            });
    };

    const deleteCampaign = (campaignId) => {
        return api.makeRequest(api.DELETE, appConfig.apiUrl + 'campaigns/' + campaignId);
    };

    return {
        getCampaigns,
        addCampaign,
        updateCampaign,
        deleteCampaign
    }
};
