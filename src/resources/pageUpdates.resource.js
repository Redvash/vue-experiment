import appConfig from '../config/app.config'

import ApplicationUpdate from '../models/applicationUpdate.model'

export default (api) => {

    const getApplicationUpdates = () => {
        return api.makeRequest(api.GET, appConfig.apiUrl + 'pageUpdates')
            .then(updates => {
                return updates.map(update => new ApplicationUpdate(update.id,update.title,update.createdAt,update.description));
            });
    };

    return {
        getApplicationUpdates
    }
};
