import appConfig from '../config/app.config'

export default (api) => {

    const authenticate = (userEmail,userPassword) => {
        let body = {
            userEmail,
            userPassword
        };
        return api.makeRequest(api.POST, appConfig.apiUrl + 'authentication/authenticate',undefined,body);
    };

    const register = (userEmail,userPassword) => {
        let body = {
            userEmail,
            userPassword
        };
        return api.makeRequest(api.POST, appConfig.apiUrl + 'authentication/register',undefined,body);
    };

    return {
        authenticate,
        register
    }
};
