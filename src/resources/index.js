//API Modules
import authentication from './authentication.resource';
import books from './books.resource';
import notes from './notes.resource';
import campaigns from './campaigns.resource';
import pageUpdates from './pageUpdates.resource'

export default (dataService) => {

    //Return api interface separated by controllers
    return {
        Authentication: authentication(dataService),
        Books: books(dataService),
        Notes: notes(dataService),
        Campaigns: campaigns(dataService),
        PageUpdates: pageUpdates(dataService)
    }
};
