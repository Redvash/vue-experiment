import appConfig from '../config/app.config'

import BookModel from '../models/book.model'

export default (api) => {

    const getUserBooks = () => {
        return api.makeRequest(api.GET, appConfig.apiUrl + 'books')
            .then(books => {
                return books.map(book =>  new BookModel(book.id,book.name,[],book.category));
            });
    };

    const addBook = (bookModel) => {
        let body = {
            name: bookModel.name,
            category: bookModel.categoryId
        };
        return api.makeRequest(api.POST, appConfig.apiUrl + 'books',undefined,body)
            .then(newBook => {
                return new BookModel(newBook.id,newBook.name,[],newBook.category);
            });
    };

    const updateBook = (bookModel) => {
        let body = {
            name: bookModel.name,
            category: bookModel.categoryId
        };
        return api.makeRequest(api.PUT, appConfig.apiUrl + 'books/' + bookModel.id, undefined, body)
            .then(updatedBook => {
                return new BookModel(updatedBook.id,updatedBook.name,[],updatedBook.category);
            });
    };

    const deleteBook = (bookId) => {
        return api.makeRequest(api.DELETE, appConfig.apiUrl + 'books/' + bookId);
    };

    return {
        getUserBooks,
        addBook,
        updateBook,
        deleteBook
    }
};
