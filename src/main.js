import 'jquery'
import 'popper.js'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min'

//Global Styles
import './assets/styles.scss'

import Vue from 'vue'

import router from './config/routes.config'
import store from './store'

import App from './App.vue'

new Vue({
    el: '#app',
    render: h => h(App),
    store,
    router
});
