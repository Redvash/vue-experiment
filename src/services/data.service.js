import axios from 'axios'

const GET_REQUEST = 'get';
const POST_REQUEST = 'post';
const PUT_REQUEST = 'put';
const DELETE_REQUEST = 'delete';

export default (store) => {
    return {
        GET: GET_REQUEST,
        POST: POST_REQUEST,
        PUT: PUT_REQUEST,
        DELETE: DELETE_REQUEST,

        makeRequest(method, url, params, body, headers) {
            if (!headers) headers = {};

            let currentAuthenticationToken = store.getters['authentication/getAuthenticationToken'];
            if(currentAuthenticationToken) {
                headers.authenticationToken = currentAuthenticationToken;
            }

            return axios({
                method: method,
                url: url,
                params: params,
                data: body,
                headers: headers
            })
                .then(handleSuccess)
                .catch(handleError);
        }
    }
}

function handleSuccess(result) {
    return result.data;
}

function handleError(error) {
    throw {
        message: error.response.data,
        request: error
    };
}
