import alertify from 'alertify.js'

const SUCCESS = 0;
const WARNING = 1;
const ERROR = 2;

alertify.logPosition("bottom left");

const logSuccess = (message,object,show) => {
	logIt(SUCCESS,message,object,show);
};
const logWarning = (message,object,show) => {
	logIt(WARNING,message,object,show);
};
const logError = (message,object,show) => {
	logIt(ERROR,message,object,show);
};

const getSuccessMessageTemplate = (message) => {
	return '<div>' +
				'<i class="fas fa-check" aria-hidden="true" style="margin-right: 10px; color: rgb(76,175,80);"></i>' +
				'<span>' + message + '</span>' +
			'</div>'
};
const getWarningMessageTemplate = (message) => {
	return '<div>' +
				'<i class="fas fa-exclamation-triangle" aria-hidden="true" style="margin-right: 10px; color: #FFCC00;"></i>' +
				'<span>' + message + '</span>' +
			'</div>'
};
const getErrorMessageTemplate = (message) => {
	return '<div>' +
				'<i class="fas fa-times" aria-hidden="true" style="margin-right: 10px; color: #FF2222;"></i>' +
				'<span>' + message + '</span>' +
			'</div>'
};

const logIt = (type,message,object,show) => {
	if(show) {
		let htmlMessage = null;
		if(type === SUCCESS) htmlMessage = getSuccessMessageTemplate(message);
		if(type === WARNING) htmlMessage = getWarningMessageTemplate(message);
		if(type === ERROR) htmlMessage = getErrorMessageTemplate(message);

		alertify.log(htmlMessage);
	}

	console.log(message,object);
};

export default {
	logSuccess,
	logWarning,
	logError
}
