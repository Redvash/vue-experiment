import _ from 'underscore';

import BookModel from "../models/book.model";
import NoteModel from "../models/note.model";

export default (api) => ({
    namespaced: true,
    state: {
        loadingBooks: false,
        loadingNotes: false,
        books: [],
        notes: []
    },
    getters: {
        books: (state) => state.books
    },
    actions: {
        //Books
        loadBooks(context) {
            context.commit('loadingBooks');

            api.Books.getUserBooks()
                .then(books => {
                    context.commit('booksLoaded', {books});
                });
        },
        addBook(context, {name, category}) {
            let newBookModel = new BookModel(null, name, [], category);

            api.Books.addBook(newBookModel)
                .then(newBookInstance => {
                    context.commit('addBook', {
                        newBook: newBookInstance
                    });
                });

        },
        removeBook(context, {bookId}) {
            api.Books.deleteBook(bookId)
                .then(() => {
                    context.commit('removeBook', {bookId});
                });
        },
        changeBookName(context, {bookId, newName}) {
            let selectedBookModelClone = _.clone(context.state.books.find(book => book.id === bookId));
            selectedBookModelClone.name = newName;

            api.Books.updateBook(selectedBookModelClone)
                .then(bookModel => {
                    context.commit('bookUpdated', {bookModel});
                });
        },
        changeBookCategory(context, {bookId, newCategoryId}) {
            let selectedBookModelClone = _.clone(context.state.books.find(book => book.id === bookId));
            selectedBookModelClone.categoryId = newCategoryId;

            api.Books.updateBook(selectedBookModelClone)
                .then(bookModel => {
                    context.commit('bookUpdated', {bookModel});
                });
        },

        //Notes
        loadNotes(context, {bookId}) {
            context.commit('loadingNotes');

            api.Notes.getBookNotes(bookId)
                .then(notes => {
                    context.commit('notesLoaded', {notes});
                });
        },
        addNote(context, {bookId, newNote}) {
            let newNoteModel = new NoteModel(null, bookId, newNote);

            api.Notes.addNote(newNoteModel)
                .then(newNote => {
                    context.commit('addNote', {newNote});
                });
        },
        editNote(context, {noteId, newNote}) {
            let selectedNoteModelClone = _.clone(context.state.notes.find(note => note.id === noteId));
            selectedNoteModelClone.note = newNote;

            api.Notes.updateNote(selectedNoteModelClone)
                .then(noteModel => {
                    context.commit('noteUpdated', {noteModel});
                });
        },
        removeNote(context, {noteId}) {
            api.Notes.deleteNote(noteId)
                .then(() => {
                    context.commit('removeNote', {noteId});
                });
        }
    },
    mutations: {
        //Books
        loadingBooks(state) {
            state.loadingBooks = true;
        },
        booksLoaded(state, {books}) {
            state.loadingBooks = false;
            state.books = books;
        },
        addBook(state, {newBook}) {
            let booksClone = state.books.slice(0);
            booksClone.push(newBook);

            state.books = booksClone;
        },
        removeBook(state, {bookId}) {
            let selectedBookIndex = state.books.findIndex(book => book.id === bookId);
            let booksClone = state.books.slice(0);
            booksClone.splice(selectedBookIndex, 1);

            state.books = booksClone;
        },
        bookUpdated(state, {bookModel}) {
            let selectedBookIndex = state.books.findIndex(book => book.id === bookModel.id);
            let booksClone = state.books.slice(0);
            booksClone[selectedBookIndex] = bookModel;

            state.books = booksClone;
        },

        //Notes
        loadingNotes(state) {
            state.loadingNotes = true;
        },
        notesLoaded(state, {notes}) {
            state.loadingNotes = false;
            state.notes = notes;
        },
        addNote(state, {newNote}) {
            let notesClone = state.notes.slice(0);
            notesClone.push(newNote);

            state.notes = notesClone;
        },
        removeNote(state, {noteId}) {
            let selectedNoteIndex = state.notes.findIndex(note => note.id === noteId);
            let notesClone = state.notes.slice(0);
            notesClone.splice(selectedNoteIndex, 1);

            state.notes = notesClone;
        },
        noteUpdated(state, {noteModel}) {
            let selectedNoteIndex = state.notes.findIndex(note => note.id === noteModel.id);
            let notesClone = state.notes.slice(0);
            notesClone[selectedNoteIndex] = noteModel;

            state.notes = notesClone;
        }
    }
});
