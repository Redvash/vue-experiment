import DummyScenario from '../fixtures/scenario'

import Enums from '../config/enums.config'
import Scenario from "../models/scenario.model";
import CombatElement from '../models/scenarioElement.model'

export default (api) => ({
    namespaced: true,
    state: {
        working: false,
        loadingScenarios: false,
        scenarios: null
    },
    getters: {
        scenarios: (state) => state.scenarios
    },
    actions: {
        loadScenarios(context, {campaignId}) {
            context.commit('loadingScenarios');

            DummyScenario.elements = DummyScenario.elements.map(element => new CombatElement(element));
            let scenarios = [new Scenario(DummyScenario)];
            scenarios.forEach(scenario => {
                scenario.elements.sort((a,b) => b.initiative - a.initiative);
            });

            context.commit('scenariosLoaded', {scenarios});
        },

        startScenariosNextTurn(context, {scenarioId}) {
            //TODO: Move logic to backend
            let selectedScenarioClone = Object.assign({},context.state.scenarios.find(scenario => scenario.id === scenarioId));
            let combatants = selectedScenarioClone.elements
                .filter(element => element.pieceType === Enums.CombatTrackerPieceType.Character || element.pieceType === Enums.CombatTrackerPieceType.Enemy)
                .sort((elementA, elementB) => elementB.initiative - elementA.initiative);
            let currentInitiative = selectedScenarioClone.activeInitiative;

            //Calculate the next active initiative
            if(currentInitiative === null || combatants[combatants.length - 1].initiative === currentInitiative) {
                selectedScenarioClone.activeInitiative = combatants[0].initiative;
            }
            else {
                for(let n = 1; n < combatants.length; n++) {
                    if (combatants[n - 1].initiative === currentInitiative && combatants[n].initiative !== currentInitiative) {
                        selectedScenarioClone.activeInitiative = combatants[n].initiative;
                        break;
                    }
                }
            }

            context.commit('scenarioUpdated', {
                scenarioModel: selectedScenarioClone
            });
        },
        addElementsToScenario(context, {scenarioId, newElements}) {
            //TODO: Move logic to backend
            let selectedScenarioClone = Object.assign({},context.state.scenarios.find(scenario => scenario.id === scenarioId));
            selectedScenarioClone.elements = selectedScenarioClone.elements.concat(newElements);
            selectedScenarioClone.elements.sort((a,b) => b.initiative - a.initiative);

            context.commit('scenarioUpdated', {
                scenarioModel: selectedScenarioClone
            });
        },
        moveElement(context, {scenarioId, elementId, newPositionX, newPositionY}) {
            //TODO: Move logic to backend
            let selectedScenarioClone = Object.assign({},context.state.scenarios.find(scenario => scenario.id === scenarioId));
            let selectedElement = selectedScenarioClone.elements.find(element => element.id === elementId);

            selectedElement.positionX = newPositionX;
            selectedElement.positionY = newPositionY;

            context.commit('scenarioUpdated', {
                scenarioModel: selectedScenarioClone
            });
        },
        removeElement(context, {scenarioId, elementId}) {
            //TODO: Move logic to backend
            let selectedScenarioClone = Object.assign({},context.state.scenarios.find(scenario => scenario.id === scenarioId));
            let selectedElementIndex = selectedScenarioClone.elements.findIndex(element => element.id === elementId);

            selectedScenarioClone.elements.splice(selectedElementIndex,1);

            context.commit('scenarioUpdated', {
                scenarioModel: selectedScenarioClone
            });
        },
        shiftScenarioGrid(context, {scenarioId, columnModifier, rowModifier}) {
            //TODO: Move logic to backend
            let selectedScenarioClone = Object.assign({},context.state.scenarios.find(scenario => scenario.id === scenarioId));

            selectedScenarioClone.elements.forEach(element => {
                element.positionX += columnModifier;
                element.positionY += rowModifier;
            });

            context.commit('scenarioUpdated', {
                scenarioModel: selectedScenarioClone
            });
        },

        addCampaign(context, {campaignModel}) {
            let newCampaignModel = new CampaignModel(campaignModel);

            context.commit('loadingCampaigns');
            return api.Campaigns.addCampaign(newCampaignModel)
                .then(newCampaignInstance => {
                    context.commit('addCampaign', {
                        newCampaign: newCampaignInstance
                    });
                });
        },
        updateCampaign(context, {campaignModel}) {
            let selectedCampaignModelClone = new CampaignModel(campaignModel);

            api.Campaigns.updateCampaign(selectedCampaignModelClone)
                .then(updatedCampaignModel => {
                    context.commit('campaignUpdated', {
                        campaignModel: updatedCampaignModel
                    });
                });
        },
        removeCampaign(context, {campaignId}) {
            api.Campaigns.deleteCampaign(campaignId)
                .then(() => {
                    context.commit('removeCampaign', {campaignId});
                });
        }
    },
    mutations: {
        loadingScenarios(state) {
            state.loadingScenarios = true;
        },
        scenariosLoaded(state, {scenarios}) {
            state.loadingScenarios = false;
            state.scenarios = scenarios;
        },
        toggleWorkingState(state , {mode}) {
            state.working = mode
        },

        addCampaign(state, {newCampaign}) {
            let campaignsClone = state.campaigns.slice(0);
            campaignsClone.push(newCampaign);

            state.campaigns = campaignsClone;
            state.loadingCampaigns = false;
        },
        removeCampaign(state, {campaignId}) {
            let selectedCampaignIndex = state.campaigns.findIndex(campaign => campaign.id === campaignId);
            let campaignsClone = state.campaigns.slice(0);
            campaignsClone.splice(selectedCampaignIndex, 1);

            state.campaigns = campaignsClone;
        },
        scenarioUpdated(state, {scenarioModel}) {
            let selectedScenarioIndex = state.scenarios.findIndex(scenario => scenario.id === scenarioModel.id);
            let scenariosClone = state.scenarios.slice(0);
            scenariosClone[selectedScenarioIndex] = scenarioModel;

            state.scenarios = scenariosClone;
        }
    }
});
