export default (api) => ({
    namespaced: true,
    state:  {
        loadingUpdates: false,
        updates: []
    },
    getters: {
        updates: (state) => state.updates
    },
    actions: {
        loadUpdates(context) {
            context.commit('loadingUpdates');

            api.PageUpdates.getApplicationUpdates()
                .then(updates => {
                    context.commit('updatesLoaded', {updates});
                });
        }
    },
    mutations: {
        loadingUpdates(state) {
            state.loadingUpdates = true;
        },
        updatesLoaded(state, {updates}) {
            state.loadingUpdates = false;
            state.updates = updates;
        }
    }
})
