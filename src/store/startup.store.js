export default {
    namespaced: true,
    state: {
        appIsReady: false
    },
    actions: {
        startup(context) {
            //Check if token already exists
            let userSession = window.localStorage.getItem('userInfo');
            if(userSession) {
                userSession = JSON.parse(userSession);
                context.commit('authentication/userAuthenticated',{
                    userInfo: userSession.user,
                    authenticationToken: userSession.token
                }, {root: true});
            }

            context.commit('changeAppState',{appIsReady: true});
        }
    },
    mutations: {
        changeAppState(state,{appIsReady}) {
            state.appIsReady = appIsReady;
        }
    }
}
