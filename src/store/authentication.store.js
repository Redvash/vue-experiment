import logger from '../services/logger.service'

export default (api) => {
    return {
        namespaced: true,
        state: {
            loading: false,
            isAuthenticated: false,
            authenticatedUser: null,
            authenticationError: null,
            authenticationToken: null
        },
        getters: {
            getAuthenticationToken: state => state.authenticationToken
        },
        actions: {
            authenticateUser(context, {userEmail, userPassword}) {
                context.commit('changingUserAuthentication');

                api.Authentication.authenticate(userEmail,userPassword)
                    .then(result => {
                        //Save session to local storage
                        window.localStorage.setItem('userInfo', JSON.stringify(result));

                        context.commit('userAuthenticated', {
                            userInfo: result.user,
                            authenticationToken: result.token
                        });
                        logger.logSuccess('User signed in successfully', result, true);
                    })
                    .catch(error => {
                        context.commit('authenticationFailed', {error})
                    });
            },
            registerUser(context, {userEmail, userPassword}) {
                context.commit('changingUserAuthentication');

                api.Authentication.register(userEmail,userPassword)
                    .then(result => {
                        //Save session to local storage
                        window.localStorage.setItem('userInfo', JSON.stringify(result));

                        context.commit('userAuthenticated', {
                            userInfo: result.user,
                            authenticationToken: result.token
                        });
                        logger.logSuccess('User registered successfully', result, true);
                    })
                    .catch(error => {
                        context.commit('authenticationFailed', {error})
                    });
            },
            logoutUser(context) {
                context.commit('changingUserAuthentication');

                //Save session to local storage
                window.localStorage.removeItem('userInfo');

                context.commit('userLoggedOut');
            }
        },
        mutations: {
            changingUserAuthentication(state) {
                state.loading = true;
                state.authenticationError = null;
            },
            userAuthenticated(state, {userInfo, authenticationToken}) {
                state.isAuthenticated = true;

                state.authenticatedUser = userInfo;
                state.authenticationToken = authenticationToken;

                state.loading = false;
            },
            userLoggedOut(state) {
                state.isAuthenticated = false;

                state.authenticatedUser = null;
                state.authenticationToken = null;

                state.loading = false;
            },
            authenticationFailed(state, {error}) {
                state.authenticationError = error;
                state.loading = false;
            }
        }
    }
};
