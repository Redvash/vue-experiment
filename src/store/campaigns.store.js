import CampaignModel from "../models/campaign.model";

export default (api) => ({
    namespaced: true,
    state: {
        loadingCampaigns: false,
        campaigns: null
    },
    getters: {
        campaigns: (state) => state.campaigns
    },
    actions: {
        loadCampaigns(context) {
            context.commit('loadingCampaigns');

            api.Campaigns.getCampaigns()
                .then(campaigns => {
                    context.commit('campaignsLoaded', {
                        campaigns
                    });
                });
        },
        addCampaign(context, {campaignModel}) {
            let newCampaignModel = new CampaignModel(campaignModel);

            context.commit('loadingCampaigns');
            return api.Campaigns.addCampaign(newCampaignModel)
                .then(newCampaignInstance => {
                    context.commit('addCampaign', {
                        newCampaign: newCampaignInstance
                    });
                });
        },
        updateCampaign(context, {campaignModel}) {
            let selectedCampaignModelClone = new CampaignModel(campaignModel);

            api.Campaigns.updateCampaign(selectedCampaignModelClone)
                .then(updatedCampaignModel => {
                    context.commit('campaignUpdated', {
                        campaignModel: updatedCampaignModel
                    });
                });
        },
        removeCampaign(context, {campaignId}) {
            api.Campaigns.deleteCampaign(campaignId)
                .then(() => {
                    context.commit('removeCampaign', {campaignId});
                });
        }
    },
    mutations: {
        loadingCampaigns(state) {
            state.loadingCampaigns = true;
        },
        campaignsLoaded(state, {campaigns}) {
            state.loadingCampaigns = false;
            state.campaigns = campaigns;
        },
        addCampaign(state, {newCampaign}) {
            let campaignsClone = state.campaigns.slice(0);
            campaignsClone.push(newCampaign);

            state.campaigns = campaignsClone;
            state.loadingCampaigns = false;
        },
        removeCampaign(state, {campaignId}) {
            let selectedCampaignIndex = state.campaigns.findIndex(campaign => campaign.id === campaignId);
            let campaignsClone = state.campaigns.slice(0);
            campaignsClone.splice(selectedCampaignIndex, 1);

            state.campaigns = campaignsClone;
        },
        campaignUpdated(state, {campaignModel}) {
            let selectedCampaignIndex = state.campaigns.findIndex(campaign => campaign.id === campaignModel.id);
            let campaignsClone = state.campaigns.slice(0);
            campaignsClone[selectedCampaignIndex] = campaignModel;

            state.campaigns = campaignsClone;
        }
    }
});
