import Vue from 'vue'
import Vuex from 'vuex'

import dataServiceInitializer from '../services/data.service'
import apiInitializer from '../resources'

import StartupModule from './startup.store'
import AuthenticationModule from './authentication.store'
import ApplicationUpdatesModules from './applicationUpdates.store'
import CampaignsModule from './campaigns.store'
import BooksModule from './books.store'
import CombatTrackerModule from './combatTracker.store'

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        startup: StartupModule
    }
});

const dataService = dataServiceInitializer(store);
const api = apiInitializer(dataService);

store.registerModule('applicationUpdates', ApplicationUpdatesModules(api));
store.registerModule('authentication', AuthenticationModule(api));
store.registerModule('campaigns', CampaignsModule(api));
store.registerModule('books', BooksModule(api));
store.registerModule('combatTracker', CombatTrackerModule(api));

export default store
