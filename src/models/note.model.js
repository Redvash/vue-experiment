import moment from 'moment'

export default class Note {

    constructor(id,bookId,note,createdDate) {
        this.id = id;
        this.bookId = bookId;
        this.note = note;
        this.createdDate = moment(createdDate);
    }

    toJson() {
        return {
            id: this.id,
            bookId: this.bookId,
            note: this.note,
            createdDate: this.createdDate.toISOString()
        }
    }

}
