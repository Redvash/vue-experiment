import moment from 'moment'

export default class Scenario {

    constructor(data) {
        this.id = typeof data.id === 'number' ? data.id : null;
        this.campaignId = typeof data.campaignId === 'number' ? data.campaignId : null;
        this.name = data.name;

        this.createdAt = typeof data.createdAt !== 'undefined' ? moment(data.createdAt) : null;
        this.updatedAt = typeof data.updatedAt !== 'undefined' ? moment(data.updatedAt) : null;

        this.elements = data.elements ? data.elements : [];

        this.activeInitiative = typeof data.activeInitiative === 'number' ? data.activeInitiative : null;

        this.gridWidth = typeof data.gridWidth === 'number' ? data.gridWidth : 50;
        this.gridHeight = typeof data.gridHeight === 'number' ? data.gridHeight : 50;
        this.backgroundImageHeight = typeof data.backgroundImageHeight === 'number' ? data.backgroundImageHeight : 1000;
        this.backgroundImageTransparency = typeof data.backgroundImageTransparency === 'number' ? data.backgroundImageTransparency : 0.6;
        this.backgroundImageOffsetTop = typeof data.backgroundImageOffsetTop === 'number' ? data.backgroundImageOffsetTop : 0;
        this.backgroundImageOffsetLeft = typeof data.backgroundImageOffsetLeft === 'number' ? data.backgroundImageOffsetLeft : 0;
    }
}
