'use strict'

import $ from 'jquery/dist/jquery.min.js'

export default class ContractModel {

    constructor(data) {
        this.id = null;
        this.name = '';
        this.price = 0;
        this.reward = 0;
        this.description = '';
        this.isFavorite = false;

        if(typeof data !== 'undefined') {
            processInputData(this,data);
        }
    }
}

function processInputData(model,data) {
    $.extend(model,data);
}