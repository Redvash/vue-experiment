import Enums from '../config/enums.config'
import CommonService from '../services/common.service'

export default class CombatElement {

    constructor(data) {

        if(!data) data = {};

        this.id = typeof data.id === 'string' ? data.id : CommonService.generateGUID();
        this.name = typeof data.name === 'string' ? data.name : '';

        this.pieceType = typeof data.pieceType === 'number' ? data.pieceType : Enums.CombatTrackerPieceType.Wall;
        this.positionX = data.positionX;
        this.positionY = data.positionY;

        //Combatant States
        this.currentHealth = data.currentHealth;
        this.maxHealth = data.maxHealth;
        this.armourClass = data.armourClass;
        this.initiative = typeof data.initiative === 'number' ? data.initiative : getInitiative(data.pieceType);
    }

    isCombatant() {
        return this.pieceType === Enums.CombatTrackerPieceType.Character ||
            this.pieceType === Enums.CombatTrackerPieceType.Enemy;
    }
}

function getInitiative(pieceType) {
    if (pieceType === Enums.CombatTrackerPieceType.Character || pieceType === Enums.CombatTrackerPieceType.Enemy) {
        return Math.round(Math.random() * (20 - 1) + 1);
    }
    else {
        return undefined;
    }

}


