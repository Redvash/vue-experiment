import moment from 'moment'

export default class Campaign {

    constructor(data) {
        this.id = typeof data.id === 'number' ? data.id : null;
        this.ownerId = typeof data.ownerId === 'number' ? data.ownerId : null;
        this.name = data.name;
        this.publicDescription = data.publicDescription;
        this.privateNotes = data.privateNotes;
        this.createdAt = typeof data.createdAt !== 'undefined' ? moment(data.createdAt) : null;
        this.updatedAt = typeof data.updatedAt !== 'undefined' ? moment(data.updatedAt) : null;
    }
}
