import moment from 'moment'

export default class ApplicationUpdate {

    constructor(id, title, updateDate, description) {
        this.id = id;
        this.title = title;
        this.updateDate = moment(updateDate);
        this.description = description;
    }

}
