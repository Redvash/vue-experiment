export default class Book {

    constructor(id, name, notes, categoryId) {
        this.id = id;
        this.name = name;
        this.notes = notes;
        this.categoryId = categoryId;
    }

    toJson() {
        return {
            id: this.id,
            name: this.name,
            categoryId: this.categoryId
        }
    }
}
