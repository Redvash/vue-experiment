const BookCategory = {
    Note: 0,
    Quest: 1,
    Character: 2,
    Diary: 3,

    properties: {
        0: {
            Id: 0,
            Name: 'Note',
            Color: '#50a7ff',
            Icon: 'fa-sticky-note'
        },
        1: {
            Id: 1,
            Name: 'Quest',
            Color: '#ff7d06',
            Icon: 'fa-question-circle'
        },
        2: {
            Id: 2,
            Name: 'Character',
            Color: '#19cd4e',
            Icon: 'fa-user'
        },
        3: {
            Id: 3,
            Name: 'Diary',
            Color: '#e1d906',
            Icon: 'fa-book'
        }
    },
};
const CombatTrackerPieceType = {
    Character: 0,
    Enemy: 1,
    Wall: 2,
    Circle: 3,
    SmallCircle: 4
};

export default {
    BookCategory,
    CombatTrackerPieceType
}
