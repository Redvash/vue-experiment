'use strict';

import Vue from 'vue'
import VueRouter from 'vue-router'

//Application Components
import HomePage from '../pages/Home.page.vue'
import BooksPage from '../pages/Books.page.vue'
import ScenariosPage from '../pages/Scenarios.page'
import Campaigns from '../pages/Campaigns.page'

Vue.use(VueRouter);

const router = new VueRouter({
    routes: [
        {
            path: '/',
            redirect: '/home'
        },
        {
            path: '/home',
            component: HomePage
        },
        {
            path: '/books',
            component: BooksPage
        },
        {
            path: '/scenarios',
            component: ScenariosPage
        },
        {
            path: '/campaigns',
            component: Campaigns
        }
    ]
});

export default router;
